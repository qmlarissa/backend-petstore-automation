# Newman - API Test Automation - Postman

## Pet Store Test Automation

The backend petstore automation is a Postman Collection in JSON. It is used to execute automatically backend test cases of the [Swagger Petstore API](https://petstore.swagger.io/). This document will show you how to execute the script/collection and generate an automatic report. Follow the instructions so you can make this project works on your machine.

The automatic case tests were based on a [manual test case execution](https://gitlab.com/qmlarissa/backend-petstore-automation/-/blob/main/Manual_TestCases_Execution.pdf).

## Generating automatic CSV report for one collection

First of all, install newman:
````
npm install -g newman-reporter-csv
````
Download the [Swagger_Petstore_Backend_S01](https://gitlab.com/qmlarissa/backend-petstore-automation/-/blob/main/Swagger_Petstore_Backend_S01)

After that you can run this command line to execute the tests and generate a CSV report:
````
newman run "C:\Users\username\Documents\Swagger_Petstore_Backend_S01" -r csv
````
The requests created on Postman will be executed sequentially and the requests will be organized like this

<p align="center">
  <img src="Comparing_the_project_to_the_report.png" width="800" title="hover text">
</p>

This is the original newman report generated:
<p align="center">
  <img src="Original_Report.PNG" width="1200" title="hover text">
</p>


By editing this CSV document, you can adapt it to your project's needs:

<p align="center">
  <img src="Test_automation_report.PNG" width="700" title="hover text">
</p>

## Generating automatic HTML report for one collection

First, install the newman-reporter-html:

````
npm install -g newman-reporter-html
````
Download the [Swagger_Petstore_Backend_S01](https://gitlab.com/qmlarissa/backend-petstore-automation/-/blob/main/Swagger_Petstore_Backend_S01)

After that you can run this command line to execute the tests and generate an HTML report:

````
newman run "C:\Users\username\Documents\Swagger_Petstore_Backend_S01" -r html
````

You can download one HTML report example [here](https://gitlab.com/qmlarissa/backend-petstore-automation/-/blob/main/Html_newman_report.html).

## Generating automatic report for multiple collections

First you will have to [install node.js](https://www.alura.com.br/artigos/instalando-nodejs-no-windows-e-linux?gclid=Cj0KCQiA15yNBhDTARIsAGnwe0XmOZm0Ztf47neIkJFMFlVj2TtgnpUnfViUAyDqd5XLdFm_LsEZ-zcaAv_nEALw_wcB)

Then dowload this [run_multiple_collections.js.txt](https://gitlab.com/qmlarissa/backend-petstore-automation/-/blob/main/Swagger_Petstore_Backend_S01), so you will be able to execute the command line to generate the report:

````
node run_multiple_collections.js.txt
````

If you want to execute your own collections you will have to input in the script .js the parametersForTestRun

<p align="center">
  <img src="fields_for_editing.png" width="400" title="hover text">
</p>

